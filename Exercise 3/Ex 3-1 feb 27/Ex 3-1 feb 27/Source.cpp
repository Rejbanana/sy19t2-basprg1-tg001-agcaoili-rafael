#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

int main() {


	float total = 0;
	float x;
	float n;
	cout << "Input number of values to be computed: " << endl;
	cin >> n;
	for (int i = 0; i < n; i++) {
		cout << "Input value " << i + 1 << ": ";
		cin >> x;


		total += x;
	}

	cout << "avg = " << total / n;

	_getch();

	return 0;
}