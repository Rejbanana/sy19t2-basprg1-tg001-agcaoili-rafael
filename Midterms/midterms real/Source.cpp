#include <iostream>
#include <string>
#include <ctime>
#include <conio.h>

using namespace std;

int main()
{
	srand(time(0));

	int heroHp, heroDamageMin, heroDamageMax, heroDamage;
	int hansHp, hansDamageMin, hansDamageMax, hansDamage;
	int hansAction;

    char action;

	// initializing hero
	cout << "Initializing Hero...";
	cout << endl;
	cout << "input HP for Hero ";
	cin >> heroHp;
	cout << "input minimum damage for Hero: ";
	cin >> heroDamageMin;
	cout << "Input maximum damage for Hero. Value must be greater than or equal to minimum damage: ";
	cin >> heroDamageMax;

	if (heroDamageMax < heroDamageMin) { // value check for hero
		while (heroDamageMax < heroDamageMin) {
			cout << "Input maximum damage for Hero. Value must be greater than or equal to minimum damage: ";
			cin >> heroDamageMax;
		}
	}

	system("CLS");
	// initializing hans
	cout << "Initializing Hans...";
	cout << endl;
	cout << "input HP for Hans ";
	cin >> hansHp;
	cout << "input minimum damage for Hans: ";
	cin >> hansDamageMin;
	cout << "Input maximum damage for Hans. Value must be greater than or equal to minimum damage: ";
	cin >> hansDamageMax;

	if (hansDamageMax < hansDamageMin) { // value check for hans
		while (hansDamageMax < hansDamageMin) {
			cout << "Input maximum damage for Hans. Value must be greater than or equal to minimum damage: ";
			cin >> hansDamageMax;
		}
	}
	system("CLS");

	while (heroHp > 0 && hansHp > 0) {
	    hansAction = rand() % (3) + 1;
		cout << "Hero's HP: " << heroHp << endl;
		cout << "Hans' HP: " << hansHp << endl;
		cout << endl;

		cout << "input your action" << endl;
		cout << "==================" << endl;
		cout << "         [a] = Attack" << endl;
		cout << "         [b] = Defend" << endl;
		cout << "         [w] = Wild Attack" << endl;
		cin >> action;

		system("CLS");
		if (action == 'a') { // hero attacks
			

			cout << "Hero Attacks" << endl;
			if (hansAction == 1) { // hans attacks

				cout << "Hans Attacks" << endl << endl << endl;

				hansDamage = rand() % (hansDamageMax - hansDamageMin + 1) + hansDamageMin;
				heroHp -= hansDamage;
				heroDamage = rand() % (heroDamageMax - heroDamageMin + 1) + heroDamageMin;
				hansHp -= heroDamage;
				
				cout << "Hero dealt " << heroDamage << " damage" << endl;
				cout << "Hans dealt " << hansDamage << " damage";
				_getch();
				
			}
			else if (hansAction == 2) {// hans defends

				cout << "Hans Defends" << endl << endl << endl;
			
				heroDamage = rand() % (heroDamageMax - heroDamageMin + 1) + heroDamageMin;
				hansHp -= (heroDamage / 2);

				cout << "Hero dealt " << heroDamage << " damage";

				_getch();
			
			}
			else if (hansAction == 3) {// hans wild attacks
				cout << "Hans prepares for a special attack" << endl << endl << endl;
				hansDamage = rand() % (hansDamageMax - hansDamageMin + 1) + hansDamageMin;
				heroHp -= (hansDamage * 2);
				heroDamage = rand() % (heroDamageMax - heroDamageMin + 1) + heroDamageMin;
				hansHp -= heroDamage;

				cout << "Hero dealt " << heroDamage << " damage" << endl;
				cout << "Hans dealt " << hansDamage << " damage. CRITICAL!";

				_getch();
			
			}
			system("CLS");

		}
		if (action == 'b') { // hero defends
			

			cout << "Hero Defends" << endl;
			if (hansAction == 1) { // hans attacks

				cout << "Hans Attacks" << endl << endl << endl;

				hansDamage = rand() % (hansDamageMax - hansDamageMin + 1) + hansDamageMin;
				heroHp -= (hansDamage / 2);
				

				cout << "Hans dealt " << hansDamage / 2 << " damage";

				_getch();
				
			}
			else if (hansAction == 2) {// hans defends

				cout << "Hans Defends" << endl << endl << endl;

				cout << "They became wary as they predict the tides of battle...";

				_getch();
			
			}
			else if (hansAction == 3) {// hans wild attacks
				cout << "Hans prepares for a special attack" << endl << endl << endl;
				
				heroDamage = rand() % (hansDamageMax - hansDamageMin + 1) + hansDamageMin;
				hansHp -= (heroDamage * 2);


				cout << "Hero: Not so fast!" << endl;
				cout << "Hero countered and dealt " << heroDamage * 2 << " damage. CRITICAL!";

				_getch();
				
			}
			system("CLS");
		}
		if (action == 'w') { // hero wild attacks
			

			cout << "Hero prepares for a special attack..." << endl;
			if (hansAction == 1) { // hans attacks

				cout << "Hans Attacks" << endl << endl << endl;

				hansDamage = rand() % (hansDamageMax - hansDamageMin + 1) + hansDamageMin;
				heroHp -= hansDamage;
				heroDamage = rand() % (heroDamageMax - heroDamageMin + 1) + heroDamageMin;
				hansHp -= (heroDamage * 2);


				cout << "Hero dealt " << heroDamage * 2 << " damage. CRITICAL!" << endl;
				cout << "Hans dealt " << hansDamage << " damage";

				_getch();
				
			}
			else if (hansAction == 2) {// hans defends

				cout << "Hans Defends" << endl << endl << endl;

				hansDamage = rand() % (heroDamageMax - heroDamageMin + 1) + heroDamageMin;
				heroHp -= (hansDamage * 2);


				cout << "Hans: Too slow!" << endl;
				cout << "Hans countered and dealt " << hansDamage * 2 << " damage.CRITICAL!";

				_getch();
				
			}
			else if (hansAction == 3) {// hans wild attacks
				cout << "Hans prepares for a special attack" << endl << endl << endl;
				hansDamage = rand() % (hansDamageMax - hansDamageMin + 1) + hansDamageMin;
				heroHp -= (hansDamage * 2);
				heroDamage = rand() % (heroDamageMax - heroDamageMin + 1) + heroDamageMin;
				hansHp -= (heroDamage * 2);


				cout << "Hans: Don't start holding back on me now!" << endl;
				cout << "Hero: I couldn't ask for more!" << endl;
				cout << "Hero dealt " << heroDamage * 2 << " damage. CRITICAL!";
				cout << "Hans dealt " << hansDamage * 2<< " damage. CRITICAL!";

				_getch();
			
			}
			system("CLS");

		}








		

		
	}
	


	if (heroHp > hansHp) {
		cout << "Hero raised his right arm as he signals his victory!" << endl << endl << endl;
		_getch();



	}
	if (heroHp < hansHp) {
		cout << "Hans looked down on Hero." << endl;
		_getch();
		cout << "Hans: You're naive to think that you can protect anything without getting blood in your hands." << endl;
		_getch();


	}
	if (heroHp <= 0 && hansHp <= 0)
	{
		cout << "Whose blade did the gods favor?" << endl;
		_getch();
		cout << "A long moment of silence..." << endl;
		_getch();
		cout << "Hero fell down as he bathes in his own blood..." << endl;
		_getch();
		cout << "Hans smiled..." << endl;
		_getch();
		cout << "Hans: Not bad...not bad..." << endl;
		_getch();
		cout << "Hans coughed blood and fell to the ground" << endl;
		_getch();
		cout << "Peace returned to the land but in exchange of the life that saved it." << endl;
		_getch();

	}



 



	return 0;
}
